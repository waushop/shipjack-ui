import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  FormGroup,
  Label,
  Input,
  Button,
  Row,
  Col,
} from "reactstrap";

import LifeboatService from "../../services/LifeboatService";

class EditLifeboat extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      lifeboatName: '',
    }
    this.saveLifeboat = this.saveLifeboat.bind(this);
    this.loadLifeboat = this.loadLifeboat.bind(this);
  }

  componentDidMount() {
    this.loadLifeboat();
  }

  loadLifeboat() {
    LifeboatService.fetchLifeboatById(window.localStorage.getItem("lifeboatId"))
      .then((res) => {
        let lifeboat = res.data;
        this.setState({
          id: lifeboat.id,
          lifeboatName: lifeboat.lifeboatName,
        })
      });
  }

  onChange = (a) =>
    this.setState({ [a.target.name]: a.target.value });

  saveLifeboat = (a) => {
    a.preventDefault();
    let lifeboat = {
      id: this.state.id,
      lifeboatName: this.state.lifeboatName,
    };
    LifeboatService.editLifeboat(lifeboat)
      .then(res => {
        this.setState({ message: 'Lifeboat added successfully.' });
        this.props.history.push('/admin/lifeboats');
      });
  }

  render() {
    return (
        <>
        <div className="content">
          <Row>
            <Col md="11">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Edit lifeboat</CardTitle>
                </CardHeader>
                <CardBody>
                  <form>
                    <div className="form-row">
                      <FormGroup className="col-md-6">
                        <Label for="lifeboatName">Name</Label>
                        <Input type="text" name="lifeboatName" id="lifeboatName" placeholder="Name" value={this.state.lifeboatName} onChange={this.onChange} />
                      </FormGroup>
                    </div>
                  </form>
                </CardBody>
              </Card>
            </Col>
            <Col md="1" className="text-center">
              <Button className="btn-round btn-icon btn btn-success btn-lg" onClick={this.saveLifeboat}><i className="fa fa-save"></i></Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default EditLifeboat;