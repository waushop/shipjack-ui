import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
  Button
} from "reactstrap";

import LifeboatService from "../../services/LifeboatService";
import $ from 'jquery';

class ListLifeboats extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      lifeboats: [],
      message: null
    }
    this.deleteLifeboat = this.deleteLifeboat.bind(this);
    this.editLifeboat = this.editLifeboat.bind(this);
    this.addLifeboat = this.addLifeboat.bind(this);
    this.reloadLifeboatList = this.reloadLifeboatList.bind(this);
  }

  componentDidMount() {
    //initialize datatable
    setTimeout(() => {
      $('#data-table').DataTable(
        {
          "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]]
        }
      );
    }, 100);
    this.reloadLifeboatList();
  }

  reloadLifeboatList() {
    LifeboatService.fetchLifeboats()
      .then((res) => {
        this.setState({ lifeboats: res.data })
      });
  }

  deleteLifeboat(lifeboatId) {
    LifeboatService.deleteLifeboat(lifeboatId)
      .then(res => {
        this.setState({ message: 'Lifeboat deleted successfully.' });
        this.setState({ lifeboats: this.state.lifeboats.filter(lifeboat => lifeboat.id !== lifeboatId) });
      })
  }

  editLifeboat(id) {
    window.localStorage.setItem("lifeboatId", id);
    this.props.history.push('/admin/edit-lifeboat');
  }

  addLifeboat() {
    window.localStorage.removeItem("lifeboatId");
    this.props.history.push('/admin/add-lifeboat');
  }

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="11">
              <Card>
                <CardHeader>
                  <Row>
                    <Col md="12">
                      <CardTitle tag="h4">Lifeboats</CardTitle>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Table id="data-table" responsive>
                    <thead className="text-secondary">
                      <tr>
                        <th>ID</th>
                        <th>Lifeboat name</th>
                        <th className="no-sort"></th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.lifeboats.map(row => (
                        <tr key={row.id}>
                          <td>{row.id}</td>
                          <td>{row.lifeboatName}</td>
                          <td className="text-right">
                            <Button className="btn-round btn-icon btn btn-warning btn-md mr-2" onClick={() => this.editLifeboat(row.id)}>
                              <i className="fa fa-pen"></i>
                            </Button>
                            <Button className="btn-round btn-icon btn btn-danger btn-md" onClick={() => this.deleteLifeboat(row.id)}>
                              <i className="fa fa-trash"></i>
                            </Button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
            <Col md="1" className="text-center">
              <Button className="btn-round btn-icon btn btn-success btn-lg" onClick={() => this.addLifeboat()}><i className="fa fa-plus"></i></Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default ListLifeboats;