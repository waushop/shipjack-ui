import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  FormGroup,
  Label,
  Input,
  Button,
  Row,
  Col,
} from "reactstrap";

import AlarmGroupService from "../../services/AlarmGroupService";

class AddAlarmGroup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      alarmGroupName: '',
      message: null
    }
    this.saveAlarmGroup = this.saveAlarmGroup.bind(this);
  }

  saveAlarmGroup = (a) => {
    a.preventDefault();
    let alarmGroup = {
      alarmGroupName: this.state.alarmGroupName,
    };
    AlarmGroupService.addAlarmGroup(alarmGroup)
      .then(res => {
        this.setState({ message: 'Alarm group added successfully.' });
        this.props.history.push('/admin/alarm-groups');
      });
  }

  onChange = (a) =>
    this.setState({ [a.target.name]: a.target.value });

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="11">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Add new alarm group</CardTitle>
                </CardHeader>
                <CardBody>
                  <form>
                    <div className="form-row">
                      <FormGroup className="col-md-6">
                        <Label for="alarmGroupName">Name</Label>
                        <Input type="text" name="alarmGroupName" id="alarmGroupName" placeholder="Name" value={this.state.alarmGroupName} onChange={this.onChange} />
                      </FormGroup>
                    </div>
                  </form>
                </CardBody>
              </Card>
            </Col>
            <Col md="1" className="text-center">
              <Button className="btn-round btn-icon btn btn-success btn-lg" onClick={this.saveAlarmGroup}><i className="fa fa-save"></i></Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default AddAlarmGroup;