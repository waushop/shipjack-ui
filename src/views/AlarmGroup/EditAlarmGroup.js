import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  FormGroup,
  Label,
  Input,
  Button,
  Row,
  Col,
} from "reactstrap";

import AlarmGroupService from "../../services/AlarmGroupService";

class EditAlarmGroup extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      alarmGroupName: '',
    }
    this.saveAlarmGroup = this.saveAlarmGroup.bind(this);
    this.loadAlarmGroup = this.loadAlarmGroup.bind(this);
  }

  componentDidMount() {
    this.loadAlarmGroup();
  }

  loadAlarmGroup() {
    AlarmGroupService.fetchAlarmGroupById(window.localStorage.getItem("alarmGroupId"))
      .then((res) => {
        let alarmGroup = res.data;
        this.setState({
          id: alarmGroup.id,
          alarmGroupName: alarmGroup.alarmGroupName,
        })
      });
  }

  onChange = (a) =>
    this.setState({ [a.target.name]: a.target.value });

  saveAlarmGroup = (a) => {
    a.preventDefault();
    let alarmGroup = {
      id: this.state.id,
      alarmGroupName: this.state.alarmGroupName,
    };
    AlarmGroupService.editAlarmGroup(alarmGroup)
      .then(res => {
        this.setState({ message: 'Alarm group saved successfully.' });
        this.props.history.push('/admin/alarm-groups');
      });
  }

  render() {
    return (
        <>
        <div className="content">
          <Row>
            <Col md="11">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Edit alarm group</CardTitle>
                </CardHeader>
                <CardBody>
                  <form>
                    <div className="form-row">
                      <FormGroup className="col-md-6">
                        <Label for="alarmGroupName">Name</Label>
                        <Input type="text" name="alarmGroupName" id="alarmGroupName" placeholder="Name" value={this.state.alarmGroupName} onChange={this.onChange} />
                      </FormGroup>
                    </div>
                  </form>
                </CardBody>
              </Card>
            </Col>
            <Col md="1" className="text-center">
              <Button className="btn-round btn-icon btn btn-success btn-lg" onClick={this.saveAlarmGroup}><i className="fa fa-save"></i></Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default EditAlarmGroup;