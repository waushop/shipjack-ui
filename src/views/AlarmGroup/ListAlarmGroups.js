import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
  Button
} from "reactstrap";

import AlarmGroupService from "../../services/AlarmGroupService";
import $ from 'jquery';

class ListAlarmGroups extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      alarmGroups: [],
      message: null
    }
    this.deleteAlarmGroup = this.deleteAlarmGroup.bind(this);
    this.editAlarmGroup = this.editAlarmGroup.bind(this);
    this.addAlarmGroup = this.addAlarmGroup.bind(this);
    this.reloadAlarmGroupsList = this.reloadAlarmGroupsList.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      $('#data-table').DataTable(
        {
          "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]]
        }
      );
    }, 100);
    this.reloadAlarmGroupsList();
  }

  reloadAlarmGroupsList() {
    AlarmGroupService.fetchAlarmGroups()
      .then((res) => {
        this.setState({ alarmGroups: res.data })
      });
  }

  deleteAlarmGroup(alarmGroupId) {
    AlarmGroupService.deleteAlarmGroup(alarmGroupId)
      .then(res => {
        this.setState({ message: 'Alarm group deleted successfully.' });
        this.setState({ alarmGroups: this.state.alarmGroups.filter(alarmGroup => alarmGroup.id !== alarmGroupId) });
      })
  }

  editAlarmGroup(id) {
    window.localStorage.setItem("alarmGroupId", id);
    this.props.history.push('/admin/edit-alarm-group');
  }

  addAlarmGroup() {
    window.localStorage.removeItem("alarmGroupId");
    this.props.history.push('/admin/add-alarm-group');
  }

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="11">
              <Card>
                <CardHeader>
                  <Row>
                    <Col md="12">
                      <CardTitle tag="h4">Alarm Groups</CardTitle>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Table id="data-table" responsive>
                    <thead className="text-secondary">
                      <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th className="no-sort"></th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.alarmGroups.map(row => (
                        <tr key={row.id}>
                          <td>{row.id}</td>
                          <td>{row.alarmGroupName}</td>
                          <td className="text-right">
                            <Button className="btn-round btn-icon btn btn-warning btn-md mr-2" onClick={() => this.editAlarmGroup(row.id)}>
                              <i className="fa fa-pen"></i>
                            </Button>
                            <Button className="btn-round btn-icon btn btn-danger btn-md" onClick={() => this.deleteAlarmGroup(row.id)}>
                              <i className="fa fa-trash"></i>
                            </Button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
            <Col md="1" className="text-center">
              <Button className="btn-round btn-icon btn btn-success btn-lg" onClick={() => this.addAlarmGroup()}><i className="fa fa-plus"></i></Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default ListAlarmGroups;