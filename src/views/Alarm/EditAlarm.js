import React from "react";
import axios from 'axios';
import Select from 'react-select'

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  FormGroup,
  Label,
  Input,
  Button,
  Row,
  Col,
} from "reactstrap";

import AlarmService from "../../services/AlarmService";

class EditAlarm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      alarmNumber: '',
      alarmGroup: '',
      lifeboat: '',
    }
    this.saveAlarm = this.saveAlarm.bind(this);
    this.loadAlarm = this.loadAlarm.bind(this);
  }

  async getLifeboatOptions(){
    const res = await axios.get('http://localhost:8080/api/lifeboats')
    const data = res.data

    const options = data.map(d => ({
      "value" : d.id,
      "label" : d.lifeboatName
    }))
    this.setState({selectLifeboatOptions: options})
  }

  async getAlarmGroupOptions(){
    const res = await axios.get('http://localhost:8080/api/alarmgroups')
    const data = res.data

    const options = data.map(d => ({
      "value" : d.id,
      "label" : d.alarmGroupName
    }))
    this.setState({selectAlarmGroupOptions: options})
  }

  componentDidMount() {
    this.loadAlarm();
    this.getLifeboatOptions()
    this.getAlarmGroupOptions()
  }

  loadAlarm() {
    AlarmService.fetchAlarmById(window.localStorage.getItem("alarmId"))
      .then((res) => {
        let alarm = res.data;
        this.setState({
          id: alarm.id,
          alarmNumber: alarm.alarmNumber,
          alarmGroup: alarm.alarmGroup,
          lifeboat: alarm.lifeboat,
        })
      });
  }

  onChange = (a) =>
    this.setState({ [a.target.name]: a.target.value });

  saveAlarm = (a) => {
    a.preventDefault();
    let alarm = {
      id: this.state.id,
      alarmNumber: this.state.alarmNumber,
      alarmGroup: this.state.alarmGroup,
      lifeboat: this.state.lifeboat,
    };
    AlarmService.editAlarm(alarm)
      .then(res => {
        this.setState({ message: 'Alarm added successfully.' });
        this.props.history.push('/admin/alarms');
      });
  }

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="11">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Edit alarm</CardTitle>
                </CardHeader>
                <CardBody>
                  <form>
                    <div className="form-row">
                      <FormGroup className="col-md-6">
                        <Label for="alarmNumber">Alarm number</Label>
                        <Input type="number" name="alarmNumber" id="alarmNumber" placeholder="Alarm number" value={this.state.alarmNumber} onChange={this.onChange} />
                      </FormGroup>
                      <FormGroup className="col-md-6">
                        <Label for="alarmGroup">Alarm group</Label>
                        <Select id="alarmGroup" options={this.state.selectAlarmGroupOptions} />
                      </FormGroup>
                    </div>
                    <div className="form-row">
                      <FormGroup className="col-md-6">
                        <Label for="lifeboat">Lifeboat</Label>
                        <Select id="lifeboat" value={this.selectedValue} options={this.state.selectLifeboatOptions} />
                      </FormGroup>
                    </div>
                    <Button color="primary" onClick={this.saveAlarm}>Save changes</Button>
                  </form>
                </CardBody>
              </Card>
            </Col>
            <Col md="1" className="text-center">
              <Button className="btn-round btn-icon btn btn-success btn-lg" onClick={this.saveAlarm}><i className="fa fa-save"></i></Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default EditAlarm;