import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
  Button
} from "reactstrap";

import AlarmService from "../../services/AlarmService";
import $ from 'jquery';

class ListAlarms extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      alarms: [],
      message: null
    }
    this.deleteAlarm = this.deleteAlarm.bind(this);
    this.editAlarm = this.editAlarm.bind(this);
    this.addAlarm = this.addAlarm.bind(this);
    this.reloadAlarmList = this.reloadAlarmList.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      $('#data-table').DataTable(
        {
          "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]]
        }
      );
    }, 100);
    this.reloadAlarmList();
  }

  reloadAlarmList() {
    AlarmService.fetchAlarms()
      .then((res) => {
        this.setState({ alarms: res.data })
      });
  }

  deleteAlarm(alarmId) {
    AlarmService.deleteAlarm(alarmId)
      .then(res => {
        this.setState({ message: 'Alarm deleted successfully.' });
        this.setState({ alarms: this.state.alarms.filter(alarm => alarm.id !== alarmId) });
      })
  }

  editAlarm(id) {
    window.localStorage.setItem("alarmId", id);
    this.props.history.push('/admin/edit-alarm');
  }

  addAlarm() {
    window.localStorage.removeItem("alarmId");
    this.props.history.push('/admin/add-alarm');
  }

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="11">
              <Card>
                <CardHeader>
                  <Row>
                    <Col md="12">
                      <CardTitle tag="h4">Alarms</CardTitle>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Table id="data-table" responsive>
                    <thead className="text-secondary">
                      <tr>
                        <th>ID</th>
                        <th>Alarm number</th>
                        <th>Alarm group</th>
                        <th>Lifeboat</th>
                        <th className="no-sort"></th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.alarms.map(row => (
                        <tr key={row.id}>
                          <td>{row.id}</td>
                          <td>{row.alarmNumber}</td>
                          <td>{row.alarmGroup.alarmGroupName}</td>
                          <td>{row.lifeboat.lifeboatName}</td>
                          <td className="text-right">
                            <Button className="btn-round btn-icon btn btn-warning btn-md mr-2" onClick={() => this.editAlarm(row.id)}>
                              <i className="fa fa-pen"></i>
                            </Button>
                            <Button className="btn-round btn-icon btn btn-danger btn-md" onClick={() => this.deleteAlarm(row.id)}>
                              <i className="fa fa-trash"></i>
                            </Button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
            <Col md="1" className="text-center">
              <Button className="btn-round btn-icon btn btn-success btn-lg" onClick={() => this.addAlarm()}><i className="fa fa-plus"></i></Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default ListAlarms;