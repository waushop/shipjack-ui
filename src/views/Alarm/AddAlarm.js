import React from "react";
import axios from 'axios';
import Select from 'react-select'

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  FormGroup,
  Label,
  Input,
  Button,
  Row,
  Col,
} from "reactstrap";

import AlarmService from "../../services/AlarmService";

class AddAlarm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      alarmNumber: '',
      alarmGroup: '',
      lifeboat: '',
      
      message: null
    }
    this.saveAlarm = this.saveAlarm.bind(this);
  }

  async getLifeboatOptions(){
    const res = await axios.get('http://localhost:8080/api/lifeboats')
    const data = res.data

    const lifeboatOtions = data.map(d => ({
      "value" : d.id,
      "label" : d.lifeboatName
    }))
    this.setState({selectLifeboatOptions: lifeboatOtions})
  }

  async getAlarmGroupOptions(){
    const res = await axios.get('http://localhost:8080/api/alarmgroups')
    const data = res.data

    const alarmGroupOptions = data.map(d => ({
      "value" : d.id,
      "label" : d.alarmGroupName
    }))
    this.setState({selectAlarmGroupOptions: alarmGroupOptions})
  }

  componentDidMount() {
    const alarmGroup = []
    this.setState({ alarmGroup });
    this.getLifeboatOptions()
    this.getAlarmGroupOptions()
  }

  saveAlarm = (a) => {
    a.preventDefault();
    let alarm = {
      alarmNumber: this.state.alarmNumber,
      alarmGroup: this.state.alarmGroup,
      lifeboat: this.state.lifeboat,
    };
    AlarmService.addAlarm(alarm)
      .then(res => {
        this.setState({ message: 'Alarm added successfully.' });
        this.props.history.push('/admin/alarms');
      });
  }

  onChange = (a) =>
    this.setState({ [a.target.name]: a.target.value });

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="11">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Add new alarm</CardTitle>
                </CardHeader>
                <CardBody>
                  <form>
                    <div className="form-row">
                      <FormGroup className="col-md-6">
                        <Label for="alarmNumber">Alarm number</Label>
                        <Input type="number" name="alarmNumber" id="alarmNumber" placeholder="Alarm number" value={this.state.alarmNumber} onChange={this.onChange} />
                      </FormGroup>
                      <FormGroup className="col-md-6">
                        <Label for="alarmGroup">Alarm group</Label>
                        <Select options={this.state.selectAlarmGroupOptions} />
                      </FormGroup>
                    </div>
                    <div className="form-row">
                      <FormGroup className="col-md-6">
                        <Label for="lifeboat">Lifeboat</Label>
                        <Select options={this.state.selectLifeboatOptions} />
                      </FormGroup>
                    </div>
                  </form>
                </CardBody>
              </Card>
            </Col>
            <Col md="1" className="text-center">
              <Button className="btn-round btn-icon btn btn-success btn-lg" onClick={this.saveAlarm}><i className="fa fa-save"></i></Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default AddAlarm;