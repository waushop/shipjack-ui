import React from "react";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Table,
  Row,
  Col,
  Button,
} from "reactstrap";

import EmployeeService from "../../services/EmployeeService";
import $ from 'jquery';

class ListEmployees extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      employees: [],
      message: null
    }
    this.deleteEmployee = this.deleteEmployee.bind(this);
    this.editEmployee = this.editEmployee.bind(this);
    this.addEmployee = this.addEmployee.bind(this);
    this.reloadEmployeeList = this.reloadEmployeeList.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      $('#data-table').DataTable(
        { 
          "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]]
        }
      );
    }, 200);
    this.reloadEmployeeList();
  }

  reloadEmployeeList() {
    EmployeeService.fetchEmployees()
      .then((res) => {
        this.setState({ employees: res.data })
      });
  }

  deleteEmployee(employeeId) {
    EmployeeService.deleteEmployee(employeeId)
      .then(res => {
        this.setState({ message: 'Employee deleted successfully.' });
        this.setState({ employees: this.state.employees.filter(employee => employee.id !== employeeId) });
      })
  }

  editEmployee(id) {
    window.localStorage.setItem("employeeId", id);
    this.props.history.push('/admin/edit-employee');
  }

  addEmployee() {
    window.localStorage.removeItem("employeeId");
    this.props.history.push('/admin/add-employee');
  }

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="11">
              <Card>
                <CardHeader>
                  <Row>
                    <Col md="12">
                      <CardTitle tag="h4">Employees</CardTitle>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <Table id="data-table" responsive>
                    <thead className="text-secondary">
                      <tr>
                        <th>ID</th>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Rank</th>
                        <th>Nationality</th>
                        <th>Alarm number</th>
                        <th className="no-sort"></th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.employees.map(row => (
                        <tr key={row.id}>
                          <td>{row.id}</td>
                          <td>{row.firstName}</td>
                          <td>{row.lastName}</td>
                          <td>{row.rank}</td>
                          <td>{row.nationality}</td>
                          <td>{row.alarm.alarmNumber}</td>
                          <td className="text-right">
                            <Button className="btn-round btn-icon btn btn-warning btn-md mr-2" onClick={() => this.editEmployee(row.id)}>
                              <i className="fa fa-pen"></i>
                            </Button>
                            <Button className="btn-round btn-icon btn btn-danger btn-md" onClick={() => this.deleteEmployee(row.id)}>
                              <i className="fa fa-trash"></i>
                            </Button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
            <Col md="1" className="text-center">
              <Button className="btn-round btn-icon btn btn-success btn-lg" onClick={() => this.addEmployee()}><i className="fa fa-plus"></i></Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default ListEmployees;