import React from "react";
import axios from 'axios';
import Select from 'react-select'

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Button,
  Label,
  FormGroup,
  Input,
  Row,
  Col,
} from "reactstrap";

import EmployeeService from "../../services/EmployeeService";

class AddEmployee extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      rank: '',
      gender: '',
      nationality: '',
      alarm: '',
		  
      message: null
    }
    this.saveEmployee = this.saveEmployee.bind(this);
  }

  async getAlarmOptions(){
    const res = await axios.get('http://localhost:8080/api/alarms')
    const data = res.data

    const alarmOtions = data.map(d => ({
      "value" : d.id,
      "label" : d.alarmNumber
    }))
    this.setState({selectAlarmOptions: alarmOtions})
  }

  componentDidMount() {
    this.getAlarmOptions()
  }

  saveEmployee = (e) => {
    e.preventDefault();
    let employee = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      rank: this.state.rank,
      gender: this.state.gender,
      nationality: this.state.nationality,
      alarm: this.state.alarm,
    };
    EmployeeService.addEmployee(employee)
      .then(res => {
        this.setState({ message: 'Employee added successfully.' });
        this.props.history.push('/admin/employees');
      });
  }

  onChange = (e) =>
    this.setState({ [e.target.name]: e.target.value });

  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="11">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Add new employee</CardTitle>
                </CardHeader>
                <CardBody>
                  <form>
                    <div className="form-row">
                      <FormGroup className="col-md-6">
                        <Label for="firstName">First name</Label>
                        <Input type="text" id="firstName" placeholder="First name" name="firstName" value={this.state.firstName} onChange={this.onChange} />
                      </FormGroup>
                      <FormGroup className="col-md-6">
                        <Label for="lastName">Last name</Label>
                        <Input type="text" id="lastName" placeholder="Last name" name="lastName" value={this.state.lastName} onChange={this.onChange} />
                      </FormGroup>
                    </div>
                    <div className="form-row">
                      <FormGroup className="col-md-6">
                        <Label for="gender">Gender</Label>
                        <Input type="select" id="gender" name="gender" value={this.state.gender} onChange={this.onChange} >
                          <option>Choose...</option>
                          <option>Male</option>
                          <option>Female</option>
                        </Input>
                      </FormGroup>
                      <FormGroup className="col-md-6">
                        <Label for="nationality">Nationality</Label>
                        <Input type="select" id="nationality" name="nationality" value={this.state.nationality} onChange={this.onChange} >
                          <option>Choose...</option>
                          <option>Estonia</option>
                          <option>Russia</option>
                        </Input>
                      </FormGroup>
                    </div>
                    <div className="form-row">
                      <FormGroup className="col-md-6">
                        <Label for="rank">Rank</Label>
                        <Input type="select" id="rank" name="rank" value={this.state.rank} onChange={this.onChange} >
                          <option>Choose...</option>
                          <option>Captain</option>
                          <option>Chief Officer</option>
                          <option>2nd Officer</option>
                          <option>3rd Officer</option>
                        </Input>
                      </FormGroup>
                      <FormGroup className="col-md-6">
                        <Label for="alarmNumber">Alarm number</Label>
                        <Select options={this.state.selectAlarmOptions} />
                      </FormGroup>
                    </div>
                  </form>
                </CardBody>
              </Card>
            </Col>
            <Col md="1" className="text-center">
              <Button className="btn-round btn-icon btn btn-success btn-lg" onClick={this.saveEmployee}><i className="fa fa-save"></i></Button>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default AddEmployee;