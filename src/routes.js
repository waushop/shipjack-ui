import Dashboard from "views/Dashboard.js";
import ListAlarms from "views/Alarm/ListAlarms"
import AddAlarm from "views/Alarm/AddAlarm"
import EditAlarm from "views/Alarm/EditAlarm"
import ListEmployees from "views/Employee/ListEmployees";
import AddEmployee from "views/Employee/AddEmployee";
import EditEmployee from "views/Employee/EditEmployee";
import ListLifeboats from "views/Lifeboat/ListLifeboats";
import AddLifeboat from "views/Lifeboat/AddLifeboat";
import EditLifeboat from "views/Lifeboat/EditLifeboat";
import ListAlarmGroups from "views/AlarmGroup/ListAlarmGroups";
import AddAlarmGroup from "views/AlarmGroup/AddAlarmGroup";
import EditAlarmGroup from "views/AlarmGroup/EditAlarmGroup";
import Login from "layouts/Auth";

var routes = [
  {
    path: "/login",
    name: "Dashboard",
    icon: "fas fa-tachometer-alt",
    component: Login,
    layout: "/",
    invisible: true,
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "fas fa-tachometer-alt",
    component: Dashboard,
    layout: "/admin",
  },
  {
    path: "/employees",
    name: "Employees",
    icon: "fas fa-users",
    component: ListEmployees,
    layout: "/admin",
  },
  {
    path: "/add-employee",
    name: "Add employee",
    icon: "fas fa-users",
    component: AddEmployee,
    layout: "/admin",
    invisible: true,
  },
  {
    path: "/edit-employee",
    name: "Edit employee",
    icon: "fas fa-users",
    component: EditEmployee,
    layout: "/admin",
    invisible: true,
  },
  {
    path: "/alarms",
    name: "Alarms",
    icon: "fas fa-cog",
    component: ListAlarms,
    layout: "/admin",
  },
  {
    path: "/add-alarm",
    name: "Alarms",
    icon: "fas fa-cog",
    component: AddAlarm,
    layout: "/admin",
    invisible: true,
  },
  {
    path: "/edit-alarm",
    name: "Alarms",
    icon: "fas fa-cog",
    component: EditAlarm,
    layout: "/admin",
    invisible: true,
  },
  {
    path: "/alarm-groups",
    name: "Alarm Groups",
    icon: "fas fa-cog",
    component: ListAlarmGroups,
    layout: "/admin",
  },
  {
    path: "/add-alarm-group",
    name: "Alarm Group",
    icon: "fas fa-cog",
    component: AddAlarmGroup,
    layout: "/admin",
    invisible: true,
  },
  {
    path: "/edit-alarm-group",
    name: "Alarm Group",
    icon: "fas fa-cog",
    component: EditAlarmGroup,
    layout: "/admin",
    invisible: true,
  },
  {
    path: "/lifeboats",
    name: "Lifeboats",
    icon: "fas fa-cog",
    component: ListLifeboats,
    layout: "/admin",
  },
  {
    path: "/add-lifeboat",
    name: "Lifeboats",
    icon: "fas fa-cog",
    component: AddLifeboat,
    layout: "/admin",
    invisible: true,
  },
  {
    path: "/edit-lifeboat",
    name: "Lifeboats",
    icon: "fas fa-cog",
    component: EditLifeboat,
    layout: "/admin",
    invisible: true,
  },
];
export default routes;
