import axios from 'axios';

const alarmGroupApiBaseUrl = 'http://localhost:8080/api/alarmgroups';

class AlarmGroupService {

    fetchAlarmGroups() {
        return axios.get(alarmGroupApiBaseUrl);
    }

    fetchAlarmGroupById(alarmGroupId) {
        return axios.get(alarmGroupApiBaseUrl + '/' + alarmGroupId);
    }

    deleteAlarmGroup(alarmGroupId) {
        return axios.delete(alarmGroupApiBaseUrl + '/' + alarmGroupId);
    }

    addAlarmGroup(alarmGroup) {
        return axios.post(""+alarmGroupApiBaseUrl, alarmGroup);
    }

    editAlarmGroup(alarmGroup) {
        return axios.put(alarmGroupApiBaseUrl + '/' + alarmGroup.id, alarmGroup);
    }

}

export default new AlarmGroupService();