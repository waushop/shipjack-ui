import axios from 'axios';

const lifeboatApiBaseUrl = 'http://localhost:8080/api/lifeboats';

class LifeboatService {

    fetchLifeboats() {
        return axios.get(lifeboatApiBaseUrl);
    }

    fetchLifeboatById(lifeboatId) {
        return axios.get(lifeboatApiBaseUrl + '/' + lifeboatId);
    }

    deleteLifeboat(lifeboatId) {
        return axios.delete(lifeboatApiBaseUrl + '/' + lifeboatId);
    }

    addLifeboat(lifeboat) {
        return axios.post(""+lifeboatApiBaseUrl, lifeboat);
    }

    editLifeboat(lifeboat) {
        return axios.put(lifeboatApiBaseUrl + '/' + lifeboat.id, lifeboat);
    }

}

export default new LifeboatService();