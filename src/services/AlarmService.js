import axios from 'axios';

const alarmApiBaseUrl = 'http://localhost:8080/api/alarms';

class AlarmService {

    fetchAlarms() {
        return axios.get(alarmApiBaseUrl);
    }

    fetchAlarmById(alarmId) {
        return axios.get(alarmApiBaseUrl + '/' + alarmId);
    }

    deleteAlarm(alarmId) {
        return axios.delete(alarmApiBaseUrl + '/' + alarmId);
    }

    addAlarm(alarm) {
        return axios.post(""+alarmApiBaseUrl, alarm);
    }

    editAlarm(alarm) {
        return axios.put(alarmApiBaseUrl + '/' + alarm.id, alarm);
    }

}

export default new AlarmService();