import axios from 'axios';

const employeeApiBaseUrl = 'http://localhost:8080/api/employees';

class EmployeeService {

    fetchEmployees() {
        return axios.get(employeeApiBaseUrl);
    }

    fetchEmployeeById(employeeId) {
        return axios.get(employeeApiBaseUrl + '/' + employeeId);
    }

    deleteEmployee(employeeId) {
        return axios.delete(employeeApiBaseUrl + '/' + employeeId);
    }

    addEmployee(employee) {
        return axios.post(""+employeeApiBaseUrl, employee);
    }

    editEmployee(employee) {
        return axios.put(employeeApiBaseUrl + '/' + employee.id, employee);
    }

}

export default new EmployeeService();