import React from "react";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";
//import { Route, Switch } from "react-router-dom";

//import routes from "routes.js";

var ps;

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            backgroundColor: "black",
            activeColor: "info",
        };
        this.mainPanel = React.createRef();
    }
    componentDidMount() {
        if (navigator.platform.indexOf("Win") > -1) {
            ps = new PerfectScrollbar(this.mainPanel.current);
            document.body.classList.toggle("perfect-scrollbar-on");
        }
    }
    componentWillUnmount() {
        if (navigator.platform.indexOf("Win") > -1) {
            ps.destroy();
            document.body.classList.toggle("perfect-scrollbar-on");
        }
    }
    componentDidUpdate(e) {
        if (e.history.action === "PUSH") {
            this.mainPanel.current.scrollTop = 0;
            document.scrollingElement.scrollTop = 0;
        }
    }
    handleActiveClick = (color) => {
        this.setState({ activeColor: color });
    };
    handleBgClick = (color) => {
        this.setState({ backgroundColor: color });
    };
    render() {
        return (
            <div class="container">
                <div class="login-container">
                    <div class="row">
                        <div class="col text-center">
                            <h2 class="mt-5">Authentication</h2>
                            <p class="lead">Login to Ship Jack</p>
                        </div>
                    </div>
                    <form method="post" action="/login" class="needs-validation" novalidate
                        autocomplete="off">
                        <div class="form-group">
                            <div class="col-md-12 mb-4">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email"
                                    placeholder="Your e-mail address" required autofocus />
                            </div>
                            <div class="col-md-12 mb-4">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password" id="password"
                                    placeholder="Password" required />
                            </div>
                            <div class="col-md-12 mb-4">
                                <button type="submit" class="btn btn-secondary btn-block mt-5" value="Login" name="submit">
                                    Login</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        );
    }
}

export default Login;
